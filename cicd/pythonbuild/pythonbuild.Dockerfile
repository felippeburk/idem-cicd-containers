FROM python:3.6-stretch

RUN pip install aspy.yaml==1.1.1 \
      cached-property==1.5.1 \
      cfgv==1.1.0 \
      identify==1.1.7 \
      importlib-metadata==0.6 \
      importlib-resources==1.0.1 \
      msgpack==0.5.6 \
      nodeenv==1.3.2 \
      pre-commit==1.12.0 \
      pyyaml==3.13 \
      six==1.11.0 \
      toml==0.10.0 \
      virtualenv==16.0.0 \
      wheel==0.32.2 && \
    apt-get update && apt-get install -y libsodium18 && \
    pip install libnacl==1.6.1

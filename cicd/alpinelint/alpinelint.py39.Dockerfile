FROM python:3.9-alpine3.14

ENV TFLINT_VERSION 0.30.0

RUN apk --update add wget python3-dev enchant2 git gcc make libc-dev openssl-dev libffi-dev \
    curl jq zeromq-dev bash nodejs npm g++ libxml2 libxml2-dev libxslt-dev shellcheck \
    terraform unzip && \
    curl -fsSL https://github.com/terraform-linters/tflint/releases/download/v${TFLINT_VERSION}/tflint_linux_amd64.zip -O && \
    unzip tflint_linux_amd64.zip && mv tflint /usr/local/bin/tflint && chmod +x /usr/local/bin/tflint && rm tflint_linux_amd64.zip && \
    pip3 install -U pip setuptools wheel && pip3 install -U \
      aspy.yaml \
      astroid \
      atomicwrites \
      attrs \
      cached-property \
      cfgv \
      coverage \
      dephell \
      identify \
      importlib-metadata \
      importlib-resources \
      isort \
      lazy-object-proxy \
      mccabe \
      modernize \
      more-itertools \
      msgpack \
      nodeenv \
      pathspec \
      pluggy \
      pre-commit \
      py \
      pycodestyle \
      pyenchant \
      pylint \
      pytest-helpers-namespace \
      pytest-tempdir \
      pytest \
      saltpylint \
      toml \
      typed-ast \
      virtualenv \
      wrapt \
      yamllint \
      pyyaml \
      six

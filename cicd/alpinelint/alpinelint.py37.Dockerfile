FROM python:3.7-alpine3.11

RUN apk --update add wget python3-dev enchant git gcc make libc-dev openssl-dev libffi-dev \
    curl jq zeromq-dev bash nodejs npm g++ libxml2 libxml2-dev libxslt-dev shellcheck && \
    pip3 install -U pip && pip3 install -U \
      aspy.yaml \
      astroid \
      atomicwrites \
      attrs \
      cached-property \
      cfgv \
      coverage \
      dephell \
      identify \
      importlib-metadata \
      importlib-resources \
      isort \
      lazy-object-proxy \
      mccabe \
      modernize \
      more-itertools \
      msgpack \
      nodeenv \
      pathspec \
      pluggy \
      pre-commit \
      py \
      pycodestyle \
      pyenchant \
      pylint \
      pytest-helpers-namespace \
      pytest-tempdir \
      pytest \
      saltpylint \
      toml \
      typed-ast \
      virtualenv \
      wheel \
      wrapt \
      yamllint \
      pyyaml \
      six

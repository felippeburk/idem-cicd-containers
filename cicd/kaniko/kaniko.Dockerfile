ARG KANIKO_VERISON

FROM gcr.io/kaniko-project/executor:debug-${KANIKO_VERSION} as kaniko

FROM python:3.8-alpine

COPY --from=kaniko /kaniko/.config /kaniko/.config
COPY --from=kaniko /kaniko/docker-credential-ecr-login /kaniko/docker-credential-ecr-login
COPY --from=kaniko /kaniko/docker-credential-gcr /kaniko/docker-credential-gcr
COPY --from=kaniko /kaniko/executor /kaniko/executor
COPY --from=kaniko /kaniko/ssl /kaniko/ssl
COPY --from=kaniko /kaniko/warmer /kaniko/warmer

RUN apk add gettext
